package Files;

import java.util.ArrayList;

public class Main {

    public static final int COUNT = 100_000;

    public static void main(String[] args) {
        Ticket[] allTickets = generate();
        NumbersIO.writeByBicycle(allTickets, "intdata.dat");

        Ticket[] sixDigitTickets = leaveOnlySixDigit(allTickets);
        NumbersIO.writeByBicycle(sixDigitTickets, "int6data.dat");

        Ticket[] luckyTickets = leaveOnlyLucky(allTickets);
        NumbersIO.writeByWriter(luckyTickets, "lucky.txt");
    }


    public static Ticket[] generate() {
        Ticket[] tickets = new Ticket[COUNT];
        int number;
        for(int i = 0; i < COUNT; i++) {
            number = 10 + (int)(Math.random() * 100_000);
            tickets[i] = new Ticket(number);
        }
        return tickets;
    }

    public static Ticket[] leaveOnlySixDigit(Ticket[] source) {
        ArrayList<Ticket> tickets = new ArrayList<>();

        for(Ticket ticket : source) {
            if(ticket.isSixDigit())
                tickets.add(ticket);
        }

        return tickets.toArray(new Ticket[tickets.size()]);
    }

    public static Ticket[] leaveOnlyLucky(Ticket[] source) {
        ArrayList<Ticket> tickets = new ArrayList<>();

        for(Ticket ticket : source) {
            if(ticket.isLucky())
                tickets.add(ticket);
        }

        return tickets.toArray(new Ticket[tickets.size()]);
    }

}

