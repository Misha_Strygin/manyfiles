package Files;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class NumbersIO {
    public static void writeByWriter(Ticket[] tickets, String path) {
        FileWriter writer = null;

        try {
            writer = new FileWriter(path);
            for (Ticket ticket : tickets) {
                writer.write(ticket.toInt() + "\n");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            writer.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public static void writeByDOS(Ticket[] tickets, String path) {
        DataOutputStream dos = null;
        try {
            dos = new DataOutputStream(new FileOutputStream(path));

            for (Ticket ticket : tickets) {
                if (ticket != null)
                    dos.writeInt(ticket.toInt());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            dos.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public static Ticket[] readByDIS(String path) {
        DataInputStream dis = null;
        Ticket[] tickets = null;

        try {
            File file = new File(path);
            dis = new DataInputStream(new FileInputStream(file));
            tickets = new Ticket[(int) file.length() / 4];

            for (int i = 0; i < tickets.length; i++) {
                tickets[i] = new Ticket(dis.readInt());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            dis.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }

        return tickets;
    }

    public static void writeByBicycle(Ticket[] tickets, String path) {
        DataOutputStream fos = null;
        try {
            fos = new DataOutputStream(new FileOutputStream(path));

            for (Ticket ticket : tickets) {
                if (ticket != null)
                    fos.write(ticket.toInt());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            fos.close();
        } catch (IOException | NullPointerException ex) {
            ex.printStackTrace();
        }
    }
}

